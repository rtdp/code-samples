
require 'net/http'
require 'uri'
require 'json'

# 
# Parses contents found over GitHub APIs
class ProfileParser

  def initialize(user_name)
    @user_name = user_name
  end

  def parse
    uri = URI.parse(github_profile_url())
    response = Net::HTTP.get_response(uri)
    if response.code == '200'
      JSON.parse(response.body)
    else
      raise 'Failed to get data from APIs'
    end
  end

  private
    def github_profile_url(ext = 'json')
      "https://github.com/#{@user_name}.#{ext}"
    end

end

