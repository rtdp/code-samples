
require_relative 'profile_parser'

# 
# calculates score from parsed data
class ScoreCalculator

  EVENT_SCORES = {
    'PushEvent' => 5,
    'PullRequestReviewCommentEvent' => 4,
    'WatchEvent' => 3,
    'CreateEvent' => 2,
    'Other' => 1
  }

  def initialize(user_name)
    @data = ProfileParser.new(user_name).parse
  end

  def total_event_score
    events = @data.map{|event_hash| event_hash['type']}
    scores = events.map do |event_type| 
      EVENT_SCORES[event_type] || EVENT_SCORES['Other'] 
    end

    # Return the sum - 
    scores.inject(:+)
  end
end
