## flipkart payment processor challenge

flipkart is a megastore and you are building the payment processor for this company.

Here are rules for the payment processor.

1. If the item purchased is a book then invoke a system that would print “mailing label”.
2. If the item purchased is a book and the book has an agent then invoke a system that would send commission to the  book agent.
3. If the item purchased is membership then upgrade the user to the “pro” status.
4. If the item purchased is a stove then invoke a system that would print safety instructions.
5. If the item purchased is a digital item then email the purchaser a link to download the item.

These are some of the rules. More and more rules are being added. Design a system which handles all such cases and is flexible enough to be extended of future.

When the code is run then on the terminal I should see

```
Printing mailing label.
Sending commission to the agent.
Upgrading the user to “pro” account.
Printing “safety instructions”
Sending email to download digital items.
```

Needless to say in this case I’m looking for design choices.

Again I'm stressing the fact that I'm looking for design choices. 
Way too many people are solving it without thinking about design 
and just making the problem produce the output. Think of it this way
how would your system work if I add additional 10, 50 or even 100 other rules.

# Solution

## Flipkart Payment Processor

A payment processor program that can scale and follows open-close principle.

To run - 

    ruby payment_processor.rb

### First few changes -

- Apart from sending commision for books which are from  agent and printing safety instructions for stove, the code also does printing of mailing address for these two items, as these are tangible items.

- Agent is renamed to Seller as I couldn't tune it well to name a module after it.


### Architecture Idea - 

There is a Product class, that forces the subclasses to implement `purchased` method.

There is a PaymentProcess. It can sell anything if it responds to `price` and `purchased` methods.

Product has many subclasses like Book, Stove, Ticket, Membership - and list can go on and on.

Then we have some mixins in lib directly, which could be included in any product subclass. Like - Tangible(for products that are tangible, it handles shipping and inventory stuff), InTangible(For intangible products, this makes sure to send download links in emails without worrying about inventory), Sellers(this can be included in any product that is going to be available from seller, handles sending seller commision).


### If I had some more time -

I would have implemented an after filter for Sellers module, thereby it would execute `pay_seller` method everytime `purchased` method is executed in the class. `pay_seller` would also handle case to make sure that seller is present for product and then only send the commisions.

This would have saved calling `pay_seller` method inside the product sub-class that supports sellers.
