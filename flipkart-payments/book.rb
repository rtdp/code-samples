require_relative 'product'
require_relative 'lib/sellers'
require_relative 'lib/tangible'

class Book < Product
  include Sellers
  include Tangible

  def initialize(id)
    @id = id 
  end

  def purchased
    pay_seller if from_seller
    ship
  end

end
