# 
# Provides private methods to class as -
# - ship
# Shipping for all intangible products.
module InTangible
  attr_accessor :email

  private
    def ship
      puts "Sending email to download digital items #{@id}"
    end
end
