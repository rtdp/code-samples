#
# Provides private methods for class as - 
# - ship
# Shipping for all tangible products.
# it also takes care of updating quantity
module Tangible
  attr_accessor :deliver_address

  private
    def ship
      handle_quantity
      puts "Printing Mailing Label for #{@id}"
    end

    def handle_quantity
      # reduce it by 1
    end
end
