require_relative 'product'

class Membership < Product

  def initialize(id)
    @id = id
  end

  def purchased
    ship
  end

  private
    def ship
      puts "Upgrading user to #{@id} account."
    end

end
