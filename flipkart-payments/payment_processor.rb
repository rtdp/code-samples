require_relative 'book'
require_relative 'membership'
require_relative 'stove'
require_relative 'ticket'

# Just sell anything that can be sold
class PaymentProcessor

  def initialize(item)
    @item = item
  end

  def purchase
    do_payments && @item.purchased
  end

  private

    def do_payments
      # get credit of @item.price
      true # confident payment gateway
    end
    
end


book = Book.new('mrutunjay')
PaymentProcessor.new(book).purchase

steve_book = Book.new('i_steve')
steve_book.from_seller = true
PaymentProcessor.new(steve_book).purchase

membership = Membership.new('pro')
PaymentProcessor.new(membership).purchase

stove = Stove.new('johnson_smith_stove')
PaymentProcessor.new(stove).purchase

ticket = Ticket.new('a_r_rehman_live')
PaymentProcessor.new(ticket).purchase

