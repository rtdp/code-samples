require_relative 'product'
require_relative 'lib/tangible'

class Stove < Product
  include Tangible

  def initialize(id)
    @id = id
  end

  def purchased
    ship
  end

  private
    def ship
      puts "Printing \"Safety Instructions\""
      super
    end
end
