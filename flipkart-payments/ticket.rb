require_relative 'product'
require_relative 'lib/in_tangible'

class Ticket < Product
  include InTangible

  def initialize(id)
    @id = id
  end

  def purchased
    ship
  end

end
