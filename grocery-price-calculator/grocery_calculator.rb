require_relative 'grocery_list'
require_relative 'price_calculator'

puts "Please enter all the items purchased separated by a comma"
groceries = GroceryList.new(gets).groceries

puts PriceCalculator.new(groceries).print
