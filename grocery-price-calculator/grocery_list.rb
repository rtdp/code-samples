
# Returns the list of valid grocery items
class GroceryList

  ITEMS = {
    milk: {
      price: 3.97,
      sale: true,
      sale_for: 2,
      sale_price: 5
    },
    bread: {
      price: 2.17,
      sale: true,
      sale_for: 3,
      sale_price: 7
    },
    banana: {
      price: 0.99,
      sale: false
    },
    apple: {
      price: 0.89,
      sale: false
    }
  }

  attr_reader :groceries
  
  def initialize(inputted_groceries)
    @all_groceries = parse_list(inputted_groceries)
    @groceries =  validated_groceries
    warn_of_invalid_input
  end

  private

    # 
    # validates the groceries against the standard list
    # to make sure only valid list is present.
    def validated_groceries
      @all_groceries.select{|i| ITEMS.keys.include?(i) }
    end

    # 
    # parses the the longs grocery list string
    # that is separated by commas
    def parse_list(inputted_groceries)
      inputted_groceries.split(',').map{|i| i.strip.downcase.to_sym }
    end

    def warn_of_invalid_input
      invalid_list = @all_groceries.select{|i| !ITEMS.keys.include?(i) }
      puts "Invalid items were entered: #{invalid_list.join(', ')}" unless
        invalid_list.empty?
    end

end
