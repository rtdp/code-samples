require_relative 'grocery_list'
require 'command_line_reporter'

# Does the price calculation, given the list of purchased items.
class PriceCalculator
  include CommandLineReporter

  def initialize(groceries_list)
    @groceries = groceries_list.inject(Hash.new(0)){|hash, grocery| hash[grocery]+=1; hash}
  end
  
  # returns hash with list of prices
  # also net and gross prices
  def calculate
    [
      create_prices_hash,
      total_net_price(create_prices_hash),
      total_gross_price(create_prices_hash)
    ]
  end

  # just prints, but nicely..
  def print
    prices, net_price, gross_price = calculate
    
    # priting work for table - 
    table(border: true) do
      row do
        column('Item', width: 20)
        column('Quantity', width: 20)
        column('Price', width: 20)
      end
      prices.each do |grocery, details|
        row do 
          column(grocery)
          column(details[:quantity])
          column(details[:gross_price])
        end
      end
    end

    # summary print - 
    puts "Total Price : $#{gross_price} "
    puts "You saved $#{discount(prices)} today. "
  end


  private

    def create_prices_hash
      @prices_hash ||= @groceries.inject(Hash.new(0)) do |prices, grocery_quantity|
        grocery, quantity = grocery_quantity
        prices[grocery] = {
          net_price:  net_price_for(grocery, quantity),
          gross_price: gross_price_for(grocery, quantity),
          quantity: quantity
        }
        prices
      end
    end

    def net_price_for(grocery, quantity)
      GroceryList::ITEMS[grocery][:price] * quantity
    end

    def gross_price_for(grocery, quantity)
      GroceryList::ITEMS[grocery][:sale] ?
        (discounted_items_price(grocery, quantity) + 
         non_discounted_items_price(grocery, quantity)) :
        net_price_for(grocery, quantity)
    end

    # sum of all grocery prices with sale applied
    def total_net_price(prices)
      @net_price ||= prices.values.
        inject(0){|sum, price_details| sum += price_details[:net_price]; sum}
    end

    # sum of all grocery prices without sale applied
    def total_gross_price(prices)
      @gross_price ||= prices.values.
        inject(0){|sum, price_details| sum += price_details[:gross_price]; sum}
    end

    def discount(prices)
      (total_net_price(prices) - total_gross_price(prices)).round(2)
    end

    def discounted_items_price(grocery, quantity)
      sale_for, price =  GroceryList::ITEMS[grocery].
        values_at(:sale_for, :sale_price)
      (quantity/sale_for) * price
    end

    def non_discounted_items_price(grocery, quantity)
      sale_for, price =  GroceryList::ITEMS[grocery].
        values_at(:sale_for, :price)
      (quantity % sale_for) * price
    end

end
