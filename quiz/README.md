## Quiz challenge

The goal is to build a quiz application. This application will be command line based and it has nothing to do with browser since the focus is design.

Quiz questions are available [here](https://www.dropbox.com/s/ktenukpy56bolbw/quiz.pdf?dl=0) .

The quiz is for class 1 student. The question can be of different format. This application should support the five formats
mentioned in the above document. However it should be extensible to support more formats. 

Please use `sqlite3` gem while building this application.

We encourage you to use https://github.com/bigbinary/wheel to start a new Rails application.
We use wheel to start any new Rails application in BigBinary. Please make first commit as the clone of the wheel after changing the name of the application. Again please make first commit as the wheel with the name change so that we can see
additional commits on top of it and in this way we will be able to see your work in commits. Some people make one commit for the whole work and that makes it extremely difficult to see what is wheel code and what is user's code.

What is a must is a good README file. We will follow the steps mentioned in README.

System should have two users: Ramu, the right man and Will, the wrong man. 

Ramu being the right man always answers all the questions right.
Will being the wrong man always answers all the questions wrong.

When I get the application I would run following commands to see the result of Ramu.

```
bundle install
rake db:create
rake setup
USER=ramu rake process_result
USER=ramu rake display_result
```

This should print following information on the screen.

```
Question 1
Capital of India is
A. Bombay
B. Calcutta
C. Madras
D. New Delhi

Ramu answerd [D] which is the right answer.

Question 2
Cricket is favorite sports in India
A. True
B. False

Ramu answerd [A] which is the right answer.

Question 3
Madan has 4 mangoes. His mother gave him 3 more mangoes. Now Madan 
has __ mangoes. He gave 2 mangoes
to his sister. Now he has ___ mangoes.

Ramu answered [7,5] which is the right answer.

Question 4
Complete the series
25,35,45, __, __, __

Ramu answered [55,65,75] which is the right answer.

Question 5
Cost of a candy is 5 cents. I have 30 cents. Select all true statements.
A. I can buy 2 candies.
B. I can buy 4 candies.
C. I can buy 6 candies.
D. I can buy 8 candies.

Ramnu answered [A, B, C] which is the right answer.
```

In rake setup you should be setting up the questions. In `process_result` you should be processing as if Ramu answered those questions and should be creating answer records. display_result_for_ramu prints the result the result.


### Same process for William ( Will )

I will repeat the process for Will which looks like this.


```
bundle install
rake db:create
rake setup
USER=will rake process_result
USER=will rake display_result
```

The result should be similar in style to what we saw with Ramu. However Will would have picked up all the wrong answers and in that case the right answer needs to be printed. For example for the question 1 it should look like


```
Question 1
Capital of India ia
A. Bombay
B. Calcutta
C. Madras
D. New Delhi

Will answerd [B] which is the wrong answer. The correct answer is [D].
```

# Solution:

## Quiz App

To run the demo - 

  ./run.sh

This file contains - 

    bundle install
    rake db:create
    rake setup

    rake process_result_for_ramu
    rake display_result_for_ramu

    rake process_result_for_will
    rake display_result_for_will


### Questions Architecture - 

- There is a main class, called Questions.

- With STI, extra classes are added as MultiOptionMultiAnswer, MultiOptionSingleAnswer, FillInBlank

- Each question type has content stored in it. And each one has it's own strict format to follow. For e.g. FillInBlank requires that question should have [#FillInBlank1(23)] included in the sentence. This will be replaced by .... when rendered in actual views. While the value in parentheses represents the actual answer.

- Each question can have many answers, and answers store fieldname as well as content of answer.

### More - 

- If I had more time, I would have added better validations to make sure that data coming to question subclasses is in exact same format as needed for that question type.

- Also, next step would have been adding the decorators ahead of models, which will spit out view compatible formatting of question content.
