class Question < ActiveRecord::Base

  validates_presence_of :content, :type

  has_many :answers

  def correct?(answers)
    self.right_answers.values.sort == answers.map(&:content).sort ?
      'right' :
      'wrong'
  end

end
