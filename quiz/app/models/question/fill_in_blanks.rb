# 
# Expected format for content - 
# The question should contain #FillInBlank<NO># 
# at the places where blank spacing showing 
# is needed.
#
# e.g. - Complete the series
# 25, 35, 45, [#FillInBlank1(ans)], [#FillInBlank2(ans)]

class Question::FillInBlanks < Question

  def right_answers
    answers = content.scan(/\[(.+?)\]/).flatten
    answers.inject(Hash.new(0)) do |hash, answer|
      hash[answer.match(/#FillInBlank\d/).to_s] =
        answer.scan(/\((\d*)\)/).flatten.first.to_s
      hash
    end
  end

end
