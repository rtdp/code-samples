#
# Expected format for content - 
# The question should contain #Option1, #Option2
# and so on.
# 
# e.g. - Mark all the answers that are true from
# following options - 
# [#Option1 West is opposite of east.]
# [#Option2 West is opposite of north.]*
#
# In this, the * at the end of line indicates the correct answer.
#
class Question::MultiChoiceMultiAnswer < Question
  include MultiChoiceAnswerFormat

end
