# 
# Expected format for content - 
# The question should contain #Option1, Option2
# and so on to indicate the options.
#
# e.g. - Ruby is better language for faster development?
# [#Option1 True]
# [#Option2 False]*
#
# Here, the * at the end of line indicates correct answer.
#
class Question::MultiChoiceSingleAnswer < Question
  include MultiChoiceAnswerFormat
end
