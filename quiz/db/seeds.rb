# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# first, users - 

User.where(email: 'ramu@exam.edu').first_or_create(
  first_name: 'ramu',
  last_name: 's',
  password: 'passme'
)

User.where(email: 'will@exam.edu').first_or_create(
  first_name: 'ramu',
  last_name: 's',
  password: 'passme'
)

# Questions - 

# MultipleChoiceSingleAnswer - 
Question::MultiChoiceSingleAnswer.create(
  content: %s{
    Capital of India is - 
    [#Option1 Bombay]
    [#Option2 Calcutta]
    [#Option3 Madras]
    [#Option4 New Delhi]*
  }
)

Question::MultiChoiceSingleAnswer.create(
  content: %s{
    Cricket is favorite sports in India -
    [#Option1 True]*
    [#Option2 False]
  }
)

Question::FillInBlanks.create(
  content: %s{
    Madan has 4 mangos. His mother gave him 3 more mangos. Now Madan has [#FillInBlank1(7)] mangos. He gave 2 mangos to his sister. Now he has [#FillInBlank2(5)] mangos.
  }
)

Question::FillInBlanks.create(
  content: %s{
    25,35,45,[#FillInBlank1(55)],[#FillInBlank2(65)]
  }
)

Question::MultiChoiceMultiAnswer.create(
  content: %s{
    Cost of candy is 5 cents. I have 30 cents. Select all the true statements - 
    [#Option1 I can buy 2 candies]*
    [#Option2 I can buy 4 candies]*
    [#Option3 I can buy 6 candies]*
    [#Option4 I can buy 8 candies]
  }
)
