module MultiChoiceAnswerFormat
  def right_answers
    get_answers_with(/\[(.*)(.*)\]\*/)
  end

  def all_answers
    get_answers_with(/\[(.*)(.*)\]/)
  end

  private
    def get_answers_with(regex)
      answers = content.scan(regex).flatten.select(&:present?)
      answers.inject(Hash.new(0)) do |hash, answer|
        hash[answer.match(/#Option\d/).to_s] =
                      answer.gsub(/#Option\d\s/, '').to_s
        hash
      end
    end

end
