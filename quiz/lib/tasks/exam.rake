
desc 'Works as exam simulator for ramu'
task process_result_for_ramu: :environment do
  ramu = User.find_by_email('ramu@exam.edu')
  Question.all.each do |question|
    question.right_answers.each do |key, value|
      question.answers.create(
        user: ramu,
        field_name: key,
        content: value
      )
    end
  end
end

def get_wrong_answer(question)
  return question.all_answers.except(*(question.right_answers.keys)).first
end

desc 'Works as exam simulator for will'
task process_result_for_will: :environment do
  will = User.find_by_email('will@exam.edu')
  Question.all.each do |question|
    if  question.instance_of?(Question::FillInBlanks)
      answer = ['#FillInBlank1', 42] #dummy wrong answer
    else
      answer = get_wrong_answer(question)
    end
    question.answers.create(
      user: will,
      field_name: answer.first,
      content: answer.last
    )
  end
end

desc 'Works to display results for ramu'
task display_result_for_ramu: :environment do
  system "rake display_result_for[ramu@exam.edu]"
end

desc 'Works to display results for will'
task display_result_for_will: :environment do
  system "rake display_result_for[will@exam.edu]"
end

desc 'displays results for given user' 
task :display_result_for, [:user_email] => :environment  do |t, args|
  user = User.find_by_email(args.user_email)
  Question.all.each_with_index do |question, index|
    answers = question.answers.where(user: user)
    puts "Question #{index+1}"
    puts question.content
    puts ""
    puts "#{user.name} answered: #{answers.map(&:content)}, which is #{question.correct?(answers)} answer."
    puts "=="*20
  end
end
